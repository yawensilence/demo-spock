package com.yawn.spock

import com.yawn.spock.service.CalculateService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Specification


/**
 *
 * @author yawn
 *     2019/6/10 16:14
 */
@SpringBootTest
class SpringBootSpec extends Specification {

    @Autowired
    CalculateService calculateService

    def "spring boot test"() {
        expect: "asas"
        z == calculateService.minus(x, y)

        where:
        x << [9, 8, 7]
        y << [6, 5, 4]
        z << [3, 3, 3]
    }


    def "spring boot test2"() {
        expect: "asas"
        z == calculateService.minus(x, y)

        where:
        x | y | z
        9 | 8 | 1
        6 | 5 | 1
        3 | 3 | 0
    }

}
