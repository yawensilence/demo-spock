package com.yawn.spock

import spock.lang.FailsWith
import spock.lang.Ignore
import spock.lang.IgnoreRest
import spock.lang.Specification
import spock.lang.Timeout
import spock.lang.Unroll

import java.util.concurrent.TimeUnit


/**
 *
 * @author yawn
 *     2019/8/28 16:59
 *
 */
class AnnotationSpec extends Specification {


    /**
     * unroll 展开的意思
     * 这个注解会将多个条件测试的结果展开，逐个展示
     * 这样我们就可以清楚知道是哪个测试用例导致测试结果失败了
     * 新版本中这个注解可以用在方法或类上面了
     *
     * 如果方法名称中使用了占位符 #a , 就可以展开的每个测试就会展示出不同的方法名字
     *
     * (http://jvm123.com/p/spock-doc/index.html#_more_on_unrolled_method_names)
     */
    @Unroll
    def "maximum of #a and #b is #c"() {
        expect:
        Math.max(a, b) == c

        where:
        a | b || c
        1 | 3 || 3
        7 | 4 || 7
        0 | 0 || 0
        1 | 2 || 2
        2 | 2 || 3
        1 | 1 || 1
    }

    @Timeout(value = 10, unit = TimeUnit.MILLISECONDS)
    def "time out limit"() {
        given:
        int sum = 110

        expect:
        for (i in 1..sum) {
            println sum
        }
    }


    @Ignore
    def "ignore test"() {
        given:
        int sum = 110

        expect:
        for (i in 1..sum) {
            println sum
        }
    }


//    @IgnoreRest
    def "IgnoreRest test"() {
        given: "sum的值"
        int sum = 110
        and: "sum2 的 值"
        int sum2 = 220

        expect:
        for (i in 1..sum) {
            println sum + sum2
        }
    }

    @FailsWith(ArithmeticException.class)
    def "fails with Exception"() {
        given:
        int a = 0

        expect:
        1 / a == 1

    }

    /**

     @Timeout
      Sets a timeout for execution of a feature or fixture method.

     @Ignore
      Ignores any feature method carrying this annotation.

     @IgnoreRest
      Any feature method carrying this annotation will be executed, all others will be ignored. Useful for quickly running just a single method.

     @FailsWith
      Expects a feature method to complete abruptly. @FailsWith has two use cases: First, to document known bugs that cannot be resolved immediately. Second, to replace exception conditions in certain corner cases where the latter cannot be used (like specifying the behavior of exception conditions). In all other cases, exception conditions are preferable.

      * 1. 记录已经知道的 bug
      * 2. 标记让方法执行失败的测试用例
      */
}
