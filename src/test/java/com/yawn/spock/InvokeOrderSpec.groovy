package com.yawn.spock

import spock.lang.Specification


/**
 *
 * @author yawn
 *     2019/8/28 17:28
 *
 */
class InvokeOrderSpec extends Specification {


    def setupSpec() {
        println ">>>>>>   setupSpec"
    }
    def setup() {
        println ">>>>>>   setup"
    }


    def "feature method"() {
        given:
        println ">>>>>> feature method <<<<<< given"
        expect:
        1 == 1
        println ">>>>>> feature method <<<<<< expect"
        cleanup:
        println ">>>>>> feature method <<<<<< cleanup"
    }

    def "feature method2"() {
        given:
        println ">>>>>> feature method2 <<<<<< given"
        expect:
        1 == 1
        println ">>>>>> feature method2 <<<<<< expect"
        cleanup:
        println ">>>>>> feature method2 <<<<<< cleanup"
    }

    def cleanup() {
        println ">>>>>>   cleanup"
    }
    def cleanupSpec() {
        println ">>>>>>   cleanupSpec"
    }
}
