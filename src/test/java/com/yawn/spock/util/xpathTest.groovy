package com.yawn.spock.xpath

import com.yawn.spock.util.XpathReader
import org.dom4j.Node

/**
 *
 * @author yawn
 *     2019/9/15 10:58
 */
XpathReader xpathReader = new XpathReader("DataInfo.XML")
Node node = xpathReader.getNode("/root/header/exchDQ")
println node.getText()
println xpathReader.getString("/root/header/exchUUID")

List<Node> list = xpathReader.getNodes("/root/data/*")
println list*.getName()
for (Node no in list) {
    println no.name
}

// node
XpathReader reader = new XpathReader("DataInfo_div.XML")
List<Node> nodeList = reader.getNodes("/root/data")
println nodeList*.getStringValue()
for (Node n in nodeList) {
    Node n1 = n.selectSingleNode("*")
    println n1.name
}
