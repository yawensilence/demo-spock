package com.yawn.spock;

import com.yawn.spock.service.CalculateService;
import org.junit.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DemoSpockApplicationTests {

    @Autowired
    CalculateService calculateService;

    @BeforeClass
//    @After
//    @AfterClass
    public static void init() {

    }

    @Before
    public void before() {

    }

    @Test
    public void test1() {
        int sum1 = calculateService.plus(4, 5);
        int sum2 = calculateService.plus(-4, 5);
        int sum3 = calculateService.plus(0, 0);
        assert sum1 == 9 : "有问题";
        assert sum2 == 1 : "有问题";
        assert sum3 == 1 : "有问题";
    }

    @Test
    public void test2() {

    }

}
