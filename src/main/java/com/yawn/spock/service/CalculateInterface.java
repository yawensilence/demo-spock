package com.yawn.spock.service;

/**
 * @author yawn
 * 2019/6/10 15:02
 */
public interface CalculateInterface {

    /**
     * +
     */
    int plus(int x, int y);

    /**
     * ++
     */
    int plusPlus(int x);

    /**
     * -
     */
    int minus(int x, int y);
}
